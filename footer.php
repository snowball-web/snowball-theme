	
			 <div class="footer">
		
					<div class="footer-column-container">
							<div class="footer-column">
										<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/snowball-logo-white.png" alt="Snowball LLC">
										<address>102 N Heights Drive<br>
										Beckley, WV 25801</address><br />
										<address>9112 Kittery Lane<br>
										Bethesda, MD 20817</address><br />
										<p><a href="tel:+1-877-331-3777">(877) 331-3777</a></p>
							</div>
								<div class="footer-column">
										<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25221.47774811911!2d-81.211339!3d37.797427899999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x884ef25e3a1f33a5%3A0xf8740a0fc079f91d!2sSnowball!5e0!3m2!1sen!2sus!4v1438886790572"></iframe>
								</div>
								<div class="footer-column">
										<h3 >Who is Snowball?</h3>
										<p>We are a full-service digital marketing company for agile business, enterprise, and local government organizations. Snowball enables businesses to prosper in today's highly competitive online world.</p>
										<div class="portfolio-button" id="footer-learn-button"><a href="index.php?page_id=5" class="portfolio-button-link">Learn more about us</a></div>
								</div>
							
					</div>
				</div> 
		

</div><!-- container -->

<?php wp_footer(); ?>
</body>
</html>