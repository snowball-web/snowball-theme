
	<?php
02
	/**
03
	 * The template for displaying 404 pages (Not Found)
04
	 *
05
	 */
06
	 
07
	get_header(); ?>
08
	 
09
	    <div id="primary" class="content-area">
10
	        <div id="content" class="site-content" role="main">
11
	 
12
	            <header class="page-header">
13
	                <h1 class="page-title"><?php _e( 'Not found', 'twentythirteen' ); ?></h1>
14
	            </header>
15
	 
16
	            <div class="page-wrapper">
17
	                <div class="page-content">
18
	                    <h2><?php _e( 'This is somewhat embarrassing, isn&rsquo;t it?', 'Snowball' ); ?></h2>
19
	                    <p><?php _e( 'Icannot find the page. Wonder where it went?', 'Snowball' ); ?></p>
20
	 
21
	                    <?php get_search_form(); ?>
22
	                     
23
	                    <h3>Check out some of our popular content:</h3>
24
	 
25
	<div class="col1">
26
	<div class="col-header">         
27
	<h3>Popular Posts</h3>
28
	</div>
29
	<?php wpp_get_mostpopular(); ?>
30
	</div>
31
	 
32
	<div class="col2">
33
	<div class="col-header">         
34
	<h3>Most Commented</h3>
35
	</div>
36
	 <?php wpp_get_mostpopular("range=all&order_by=comments"); ?>
37
	</div>
38
	 
39
	<div class="col3">
40
	<div class="col-header">         
41
	<h3>Recent Posts</h3>
42
	</div>
43
	 <?php wp_get_archives( array( 'type' => 'postbypost', 'limit' => 10, 'format' => 'custom', 'before' => '', 'after' => '<br />' ) ); ?>
44
	</div>
45
	                </div><!-- .page-content -->
46
	            </div><!-- .page-wrapper -->
47
	 
48
	        </div><!-- #content -->
49
	    </div><!-- #primary -->
50
	 
51
	<?php get_footer(); ?>