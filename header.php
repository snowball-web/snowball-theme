<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, inital-scale="1" />
		<title><?php bloginfo('name'); ?></title>
		<?php wp_head(); ?>
	</head>
	
<body <?php body_class(); ?>>
	<header role="banner">
		<div class="TOP">
			<a href=<?php echo home_url() ?>><img src="<?php echo get_template_directory_uri(); ?>/images/snowball-logo.png" class="alignleft"/></a>
			
			<div class="alignright">
					<div class="social-column">
						<a href="http://snowball-web.com/blog.html"><img src="<?php echo get_template_directory_uri(); ?>/images/blog.png"/></a>
						
					</div>
					<div class="social-column">
						<a href="https://www.facebook.com/SnowballWeb"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png"/></a>
						
					</div>
					<div class="social-column">
						<a href="https://www.twitter.com/SnowballWeb"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png"/></a>
						
					</div>
				</div>
			</div>
		
				
				<nav class="site-nav" role="navigation">
					
					<?php
					
					$args = array(
						'theme_location' => 'primary'
					);
					
					?>
					
					<?php wp_nav_menu(  $args ); ?>
				</nav>
			</header>
			
		
	
	<div class="page">
	